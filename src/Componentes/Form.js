import React from "react";
import '../css/Form.css'



const envio = () =>{
  const alerta = document.getElementById('alerta')
  alerta.style.display="block";
}

function Form() {
  return (
    <section className="container-form">
      <h2>Contacto</h2>
      <form onSubmit={envio} className="form" action="https://formsubmit.co/b77fd8d19eb39021e295dd87f105e21b" method="post">
        <p>Nombre</p>
        <input type="text" name="name" placeholder="Escribe un nombre..." className="input" required />
        <p>Correo</p>
        <input type="email" name="email" placeholder="Escribe un correo..." className="input" required/>
        <p>Mensaje</p>
        <textarea className="input" name="comments" placeholder="Escribe tu mensaje..." required>
        </textarea>
        <button onSubmit={envio}>ENVIAR</button>
        <input type="hidden" name="_next" value="https://portfoliomarco.vercel.app/" />
        <input type="hidden" name="_captcha" value="false"/>
      </form>
      <div className="alerta" id="alerta">
        <h3>Correo enviado</h3>
        <p>Te contactaremos pronto</p>
      </div>
    </section>
  );
}

export default Form;
