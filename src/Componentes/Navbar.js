import '../css/Navbar.css'

function Navbar() {
  return (
    <header className="header">
      <div className="logo">Portfolio</div>
      <nav className="nav-bar">
        <a href="#inicio">INICIO</a>
        <a href="#proyectos">PROYECTOS</a>
        <a href="#contacto">CONTACTO</a>
      </nav>
    </header>
  );
}

export default Navbar;
