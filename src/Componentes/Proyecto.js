import '../css/Proyecto.css'

function Proyecto ( { image, titulo, descripcion, link } ){
    return(
        <section className='pr-container'>
            
            <div className='img-container-pr' style={ {
                background:`url(${image}) no-repeat`,
                backgroundSize:'contain',
                backgroundPosition:'top center'
                } } >
            </div>
            <h3 className='title-pr'>
                {titulo}
            </h3>
            <p className='p-pr'>{descripcion} </p>
            <button className='btn-pr'><a href={link} target='_blank'>VER MÁS</a></button>
        </section>
    )
}

export default Proyecto;