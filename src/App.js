import "./App.css";
import Navbar from "./Componentes/Navbar";
import Proyecto from "./Componentes/Proyecto";
import Form from "./Componentes/Form";
import ui from "./img/ui.svg";
import formImg from "./img/form-img.svg";
import blogr from './img/blogr.png';
import card from './img/card.png'
import todo from './img/todo.png'
import tipCalculator from './img/tip-calculator.png'
import profileCard from './img/porfile-card.png'
import sanMed from './img/sanmed.png'
import snap from './img/snap.png'


const proyectos = [
  {
    img: tipCalculator,
    title: "Tip Calculator",
    descr: "Calculadora de propinas realizada con html, css y Javascript",
    link: "https://resplendent-gecko-b1d5e6.netlify.app",
  },
  {
    img: card,
    title: "Formulario Tarjeta",
    descr: "Formulario de tarjeta realizada con html, css y Javascript",
    link: "https://coruscating-crostata-298b82.netlify.app/",
  },
  {
    img: todo,
    title: "Todo list",
    descr: "Todo list realizada en React",
    link: "https://todo-app-sooty-omega.vercel.app/",
  },
  {
    img: blogr,
    title: "Blogr",
    descr: "Landing page con html, css y Javascript",
    link: "https://heroic-ganache-b38123.netlify.app/",
  },
  {
    img: profileCard,
    title: "Profile Card",
    descr: "Componente de una ficha de perfil realizada en React",
    link: "https://profile-card-component-liart.vercel.app/",
  },
  {
    img: sanMed,
    title: "SanMed",
    descr: "Diseño y maquetación de landing page de sitio médico",
    link: "https://precious-boba-6c80d3.netlify.app/",
  }
];

function App() {
  return (
    <div className="App">
      <Navbar />
      <main className="main-container" id="inicio">
        <section className="description-container">
          <h1>Hola, soy Marco Esparza</h1>
          <h2>Desarrollador FrontEnd</h2>
          <p>
            Enfocado en el detalle, con determinación en la resolucion de
            problemas, buena organización y manejo de objetivos
          </p>
        </section>
        <div className="ui-container">
          <img src={ui} />
        </div>
      </main>
      <nav className="redes-container">
        <a href="https://gitlab.com/marcoblas" target="_blank">
          <i class="fa-brands fa-gitlab"></i>
        </a>
        <a href="https://www.linkedin.com/in/marcoeblas/" target="_blank">
          <i class="fa-brands fa-linkedin-in"></i>
        </a>
        <a href="mailto:marco.1996.blas@gmail.com">
          <i class="fa-solid fa-envelope"></i>
        </a>
      </nav>
      <section className="proyect-container" id="proyectos">
        <h2>Proyectos</h2>
        <div className="container-components">
          {proyectos.map(proyecto=>{
            return <Proyecto image={proyecto.img} titulo={proyecto.title} descripcion={proyecto.descr} link={proyecto.link}/>
          })}
        </div>
      </section>
      <section className="tecnologias-container">
        <h2>Tecnologías</h2>
        <div className="icons-main-container">
          <div className="icon-container">
            <i class="fa-brands fa-html5" style={{ color: "#e44d26" }}></i>
            <p>Html</p>
          </div>
          <div className="icon-container">
            <i class="fa-brands fa-css3-alt" style={{ color: "#264de4" }}></i>
            <p>Css</p>
          </div>
          <div className="icon-container">
            <i class="fa-brands fa-js" style={{ color: "#f0db4f" }}></i>
            <p>Javascript</p>
          </div>
          <div className="icon-container">
            <i class="fa-brands fa-react" style={{ color: "#00d8ff" }}></i>
            <p>React</p>
          </div>
          <div className="icon-container">
            <i class="fa-brands fa-sass" style={{ color: "#cf649a" }}></i>
            <p>Sass</p>
          </div>
          <div className="icon-container">
            <i class="fa-brands fa-git-alt" style={{ color: "#f03c2e" }}></i>
            <p>Git</p>
          </div>
        </div>
      </section>
      <section className="container-form-main" id="contacto">
        <Form />
        <div className="form-img">
          <img src={formImg} />
        </div>
      </section>
      <footer class="footer">
      <p>Diseñado y desarrollado por: Marco Esparza</p>
      <nav className="redes-container-footer">
        <a href="https://gitlab.com/marcoblas" target="_blank">
          <i class="fa-brands fa-gitlab"></i>
        </a>
        <a href="https://www.linkedin.com/in/marcoeblas/" target="_blank">
          <i class="fa-brands fa-linkedin-in"></i>
        </a>
        <a href="mailto:marco.1996.blas@gmail.com">
          <i class="fa-solid fa-envelope"></i>
        </a>
      </nav>
    </footer>
    </div>
  );
}

export default App;
